from tastypie.resources import ModelResource
# from tastypie.authorization import Authorization
from tastypie.authorization import *
from tastypie.authentication import *
from tastypie import fields
from myapp.models import *


class EmpresaResource(ModelResource):
    class Meta:
        queryset = Empresa.objects.all()
        resource_name = 'empresa'
        # excludes = ['cnpj']


class MaterialResource(ModelResource):
    empresa = fields.ForeignKey(EmpresaResource, attribute='importador', full=True)
    class Meta:
        queryset = Material.objects.all()
        resource_name = 'material'
        # allowed_methods = ['get']


class RTMResource(ModelResource):
    material = fields.ToManyField(MaterialResource, attribute='material')
    empresa = fields.ForeignKey(EmpresaResource, attribute='destinatario', full=True)
    class Meta:
        queryset = RTM.objects.all()
        # resource_name = 'rtm'
        allowed_methods = ['get', 'post', 'put']
        authorization = DjangoAuthorization()
        authentication = ApiKeyAuthentication()
