# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Empresa'
        db.create_table('myapp_empresa', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cnpj', self.gf('django.db.models.fields.CharField')(max_length=14)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('myapp', ['Empresa'])

        # Adding model 'Material'
        db.create_table('myapp_material', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('importador', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['myapp.Empresa'])),
        ))
        db.send_create_signal('myapp', ['Material'])

        # Adding model 'RTM'
        db.create_table('myapp_rtm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numero', self.gf('django.db.models.fields.IntegerField')()),
            ('destinatario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['myapp.Empresa'])),
            ('data_cadastro', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 6, 7, 0, 0))),
        ))
        db.send_create_signal('myapp', ['RTM'])

        # Adding M2M table for field material on 'RTM'
        m2m_table_name = db.shorten_name('myapp_rtm_material')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('rtm', models.ForeignKey(orm['myapp.rtm'], null=False)),
            ('material', models.ForeignKey(orm['myapp.material'], null=False))
        ))
        db.create_unique(m2m_table_name, ['rtm_id', 'material_id'])


    def backwards(self, orm):
        # Deleting model 'Empresa'
        db.delete_table('myapp_empresa')

        # Deleting model 'Material'
        db.delete_table('myapp_material')

        # Deleting model 'RTM'
        db.delete_table('myapp_rtm')

        # Removing M2M table for field material on 'RTM'
        db.delete_table(db.shorten_name('myapp_rtm_material'))


    models = {
        'myapp.empresa': {
            'Meta': {'object_name': 'Empresa'},
            'cnpj': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'myapp.material': {
            'Meta': {'object_name': 'Material'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['myapp.Empresa']"}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'myapp.rtm': {
            'Meta': {'object_name': 'RTM'},
            'data_cadastro': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 6, 7, 0, 0)'}),
            'destinatario': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['myapp.Empresa']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'material': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['myapp.Material']", 'symmetrical': 'False'}),
            'numero': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['myapp']