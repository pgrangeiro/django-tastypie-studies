# -*- coding: utf-8 -*-
from django.db import models
import datetime


class Empresa(models.Model):
    cnpj = models.CharField(u'CNPJ', max_length=14)
    nome = models.CharField(u'Nome', max_length=50)

    class Meta:
        verbose_name = u'Empresa'
        verbose_name_plural = u'Empresas'

    def __unicode__(self):
        return u'Empresa %s' % self.pk


class Material(models.Model):
    codigo = models.CharField(u'Código', max_length=10)
    nome = models.CharField(u'Nome', max_length=50)
    importador = models.ForeignKey(Empresa)

    class Meta:
        verbose_name = u'Material'
        verbose_name_plural = u'Materiais'

    def __unicode__(self):
        return u'Material %s' % self.pk

    @property
    def empresa(self):
        return self.importador.__unicode__()


class RTM(models.Model):
    numero = models.IntegerField(u'Número')
    destinatario = models.ForeignKey(Empresa)
    material = models.ManyToManyField(Material)
    data_cadastro = models.DateTimeField(u'Data de cadastro', default=datetime.datetime.now())

    class Meta:
        verbose_name = u'RTM'
        verbose_name_plural = u'RTMs'

    def __unicode__(self):
        return u'RTM %s' % self.pk
