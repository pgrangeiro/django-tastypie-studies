from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from myapp.api import *
from tastypie.api import Api

admin.autodiscover()
# rtm_resource = RTMResource()
v1_api = Api(api_name='v1')
v1_api.register(EmpresaResource())
v1_api.register(MaterialResource())
v1_api.register(RTMResource())

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^api/', include(rtm_resource.urls)),
    url(r'^api/', include(v1_api.urls)),
)
